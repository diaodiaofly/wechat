var WxParse = require('../../utils/wxParse.js');
var app = getApp();
Page({
  data: {
    title: null, content: null, id: null, desc: null, mobile: '18673911388'
  },
  onLoad: function (options) {
    var id = options.id;
    var that = this;
    wx.request({
      url: "https://seejoke.com/wechatapi/new/" + id,
      data: { sign: getApp().data.sign },
      method: 'GET',
      success: function (res) {
        that.setData({
          id: id,
          title: res.data.title,
          content: res.data.content,
          desc: res.data.content_desc
        })
        WxParse.wxParse('content', 'html', that.data.content, that, 25);
      }
    });
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  onShareAppMessage: function () {
    return {
      title: this.data.title,
      desc: this.data.desc,
      path: '/pages/news/news-details?id=' + this.data.id
    }
  },
  callWe: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.mobile
    })
  }
})