var url = "https://seejoke.com/wechatapi/produt";
var getDataList = function (that) {
  that.setData({
    hidden: false
  });
  var pageNum = that.data.pageNum;
  wx.request({
    url: url,
    data: {
      'sign': getApp().data.sign,
      'pageNum': pageNum
    },
    success: function (res) {
      var l = that.data.news;
      for (var i = 0; i < res.data.data.length; i++) {
        l.push(res.data.data[i])
      }
      that.setData({
        news: l,
        pageNum: pageNum + 1
      });
      that.setData({
        hidden: true
      });
    }
  });
}
Page({
  data: {
    'title': '公司服务项目',
    'news': [],
    'hidden': false
  },
  onLoad: function (options) {
    this.setData({
      pageNum: options.p
    });
    var that = this;
    getDataList(that);
  },
  onPullDownRefresh: function () {
    this.setData({
      news: []
    });
    var that = this;
    getDataList(that);
  }, onReachBottom: function () {
    var that = this;
    getDataList(that);
  }, onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  go: function (event) {
    wx.navigateTo({
      url: '/pages/news/news-details?id=' + event.currentTarget.dataset.type
    })
  }
})