var appInstance = getApp();
Page({
  userName: function (e) {
    var that = this;
    that.setData({
      userName: e.detail.value
    })
  },
  data: {
    result: 1,
    userName: null,
    email: null,
    mobile: null
  }, userName: function (e) {
    var that = this;
    that.setData({
      userName: e.detail.value
    })
    var userName = that.data.userName
    if (userName == '') {
      wx.showToast({
        title: '请输入姓名',
        icon: 'none',
        //image: '../Image/error.png',
        duration: 2000
      })
    }
  }, mobile: function (e) {
    var that = this;
    that.setData({
      mobile: e.detail.value
    })
    var mobile = that.data.mobile
    if (mobile.length != 11) {
      wx.showToast({
        title: '请输入正确的电话号码',
        icon: 'none',
        //image: '../Image/error.png',
        duration: 2000
      })
    }
  }, onLoad: function () {
    console.log("hi");
  }, formSubmit: function (e) {
    var that = this;
    var userName = e.detail.value.userName;
    var mobile = e.detail.value.mobile;
    var email = e.detail.value.email;
    var content = e.detail.value.content;
    if (userName == '' || email == '' || mobile == '') {
      wx.showToast({
        title: '请输入必填项',
        icon: 'none',
        //image: '../Image/error.png',
        duration: 2000
      })
    } else {
      wx.request({
        method: 'POST',
        url: 'https://seejoke.com/wechatapi/new/1?token=seejoke.com',
        data: {
          'userName': userName,
          'email': email,
          'mobile': mobile,
          'content': content
        },
        header: { 'content-type': 'application/json' },
        success: function (res) {
          if (res.data.status == '200') {
            console.log(res);
            wx.showToast({
              title: '提交成功',
              //image: '../Image/suess.png',
              duration: 2000
            })
            setTimeout(function () {
              wx.switchTab({
                url: '../product/product',
              })
            }, 2000)
          } else {
            wx.showToast({
              title: '提交失败',
              icon: 'none',
              //image: '../Image/suess.png',
              duration: 2000
            })
          }
        },
        fail: function (res) {
          console.log('error' + ':' + res)
        }
      })
    }
  }, onShareAppMessage: function () {
    return {
      title: '湖南湘艺来园林绿化工程有限公司',
      desc: '在线咨询，提供一流的园林绿化工程设计与实施服务',
      path: '/pages/online/index'
    }
  }
})