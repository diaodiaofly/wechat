Page({
  data: {
    title: '联系我们',
    latitude: 31.838403,
    longitude: 120.528958,
    scale: 16,
    mobile: '18673911388',
    markers: [{
      latitude: 31.838403,
      longitude: 120.528958,
      name: '湖南省邵阳市'
    }]
  }, onShareAppMessage: function () {
    console.log('分享')
    return {
      title: '湘艺来园林绿化工程有限公司',
      desc: '是一家集个性化设计和专业化施工为一体的特色园林景观公司',
      path: '/pages/about/index'
    }
  }, callWe: function () {
    wx.makePhoneCall({
      phoneNumber: this.data.mobile
    })
  }
})