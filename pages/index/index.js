Page({
  data: {
    title: '湖南湘艺来园林绿化工程有限公司',
    swipers: [
      { 'pic': '/images/index_home.jpg', 'link': '/pages/about/index' }
    ],
    news: [],
    indicatorDots: false,
    autoplay: false,
    interval: 5000,
    duration: 1000
  },
  onLoad: function () {
    var that = this;
    wx.request({
      url: "https://seejoke.com/wechatapi/produt",
      data: { sign: getApp().data.sign },
      method: 'GET',
      success: function (res) {
        that.setData({
          news: res.data.data,
        })
      }
    });
  },
  onShareAppMessage: function () {
    return {
      title: '湘艺来园林绿化设计',
      desc: '注册资金2000万元。是一家集个性化设计和专业化施工为一体的特色园林景观公司',
      path: '/pages/index/index'
    }
  },
  onShow: function () {
    var that = this
    var uid = wx.getStorageSync('uid')
  },
});