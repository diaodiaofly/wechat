Page({
  data: {
    'products': [
      {
        'id': 100,
        'pic': '/uploads/2016.jpg',
        'title': '测试标题',
        'content': '测试内容'
      }],
    'services': [
      {
        'id': 1000,
        'pic': '/uploads/2016.jpg',
        'title': '标题',
        'content': '测试内容'
      }
    ]
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  go: function (event) {
    wx.navigateTo({
      url: '/pages/news/news-details?id=' + event.currentTarget.dataset.type
    })
  }
})